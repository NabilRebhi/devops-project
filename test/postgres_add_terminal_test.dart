import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/postgresutil/postgres_terminal_services.dart';

void main() {

  test('test add terminal', () async {
    String result = "none";

    await PostgresTerminalService.addTerminal(
            ipAddress: '0.0.0.0', name: 'test_name', location: 'test_location')
        .then((value) => result = value);
    expect(result, "done");
  });

}
