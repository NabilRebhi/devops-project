import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/screens/signin.dart';
import 'package:flutter/material.dart';

void main() {
  testWidgets('Signin page has no errors', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignInPage()));
    expect(find.byType(SignInPage), findsOneWidget);
  });

}
