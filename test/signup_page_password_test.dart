import 'package:flutter_test/flutter_test.dart';

import 'package:simplemonitor/screens/signup.dart';
import 'package:flutter/material.dart';

void main() {
  

  testWidgets('Repeated password field validation', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignUpPage()));

    // Enter a password in the password field
    await tester.enterText(find.byType(TextFormField).at(2), '12345678');

    // Enter a different password in the repeated password field
    await tester.enterText(find.byType(TextFormField).at(3), '87654321');
        // Tap the "Sign Up" button
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();

    // Check that an error message is displayed
    expect(find.text('The passwords do not match !'), findsOneWidget);

    // Enter the same password in the repeated password field
    await tester.enterText(find.byType(TextFormField).at(3), '12345678');

    // Tap the "Sign Up" button again
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();

    // Check that the error message is not displayed anymore
    expect(find.text('The passwords do not match !'), findsNothing);
  });

 

}

