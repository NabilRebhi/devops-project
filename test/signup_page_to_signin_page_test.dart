import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/screens/signin.dart';
import 'package:simplemonitor/screens/signup.dart';
import 'package:flutter/material.dart';

void main() {

  testWidgets('should navigate to SignInPage when pressing the SignIn button',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignUpPage()));

    expect(find.byType(SignInPage), findsNothing);

    await tester.tap(find.text('SignIn'));
    await tester.pumpAndSettle();

    expect(find.byType(SignInPage), findsOneWidget);
  });

}

