import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/postgresutil/postgres_terminal_services.dart';

void main() {

  test('test delete terminal', () async {
    String result = "none";

    await PostgresTerminalService.getTerminals()
        .then((value) => result = value);
    expect(result, "done");
  });
}
