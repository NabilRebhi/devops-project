import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/screens/signin.dart';
import 'package:flutter/material.dart';
void main() {
  

  testWidgets('passowrd field validation', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignInPage()));

    // Enter an empty fields
    await tester.enterText(find.byType(TextFormField).last, '');

    // Tap the "Sign In" button
    await tester.tap(find.text('SignIn'));
    await tester.pumpAndSettle();

    // Check that an error message is displayed
    expect(find.text('Empty Password Field!'), findsOneWidget);
    // Enter a non empty fields
    await tester.enterText(find.byType(TextFormField).last, 'test_password');
    // Tap the "Sign In" button
    await tester.tap(find.text('SignIn'));
    await tester.pumpAndSettle();

    // Check that the error message is not displayed anymore
    expect(find.text('Empty Password Field!'), findsNothing);
  });

}
