import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/screens/signin.dart';
import 'package:flutter/material.dart';


void main() {
  testWidgets('Username field validation', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignInPage()));

    // Enter an empty fields
    await tester.enterText(find.byType(TextFormField).first, '');

    // Tap the "Sign In" button
    await tester.tap(find.text('SignIn'));
    await tester.pumpAndSettle();

    // Check that an error message is displayed
    expect(find.text('Empty Username Field!'), findsOneWidget);

    await tester.enterText(find.byType(TextFormField).first, 'test_username');

    // Tap the "Sign In" button
    await tester.tap(find.text('SignIn'));
    await tester.pumpAndSettle();

    // Check that an error message is displayed
    expect(find.text('Empty Username Field!'), findsNothing);
  });

}
