import 'package:flutter_test/flutter_test.dart';

import 'package:simplemonitor/screens/signup.dart';
import 'package:flutter/material.dart';

void main() {
  testWidgets('SignUpPage has no errors', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignUpPage()));
    expect(find.byType(SignUpPage), findsOneWidget);
  });


}

