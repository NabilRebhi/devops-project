import 'package:flutter_test/flutter_test.dart';

import 'package:simplemonitor/screens/signup.dart';
import 'package:flutter/material.dart';

void main() {

  testWidgets('Email field validation', (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignUpPage()));

    // Enter an invalid email address
    await tester.enterText(find.byType(TextFormField).at(1), 'invalid_email');

    // Tap the "Sign Up" button
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();

    // Check that an error message is displayed
    expect(find.text('Email not valid !'), findsOneWidget);

    // Enter a valid email address
    await tester.enterText(find.byType(TextFormField).at(1), 'valid@email.com');

    // Tap the "Sign Up" button again
    await tester.tap(find.text('Submit'));
    await tester.pumpAndSettle();

    // Check that the error message is not displayed anymore
    expect(find.text('Email not valid !'), findsNothing);
  });


}

