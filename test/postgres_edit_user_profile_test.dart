import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';

void main() {
  test('test editing profile', () async {
    String result = await PostgresClientService.editProfile(
        previousUsername: 'test_user',
        newUsername: 'test_user',
        email: 'test@test-email.com',
        password: 'test_password');
    expect(result, 'done');
  });

  
}
