import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';

void main() {
  test('test creation of the tables', () async {
    String result = "none";

    await PostgresClientService.checkQueriesAfterCreatingTables()
        .then((value) => result = value);
    expect(result, "done");
  });
  
}
