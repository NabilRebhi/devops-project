import 'package:flutter_test/flutter_test.dart';
import 'package:simplemonitor/screens/signin.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/screens/signup.dart';

void main() {
  testWidgets('should navigate to SignUpPage when pressing the SignUp button',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MaterialApp(home: SignInPage()));

    expect(find.byType(SignUpPage), findsNothing);

    await tester.tap(find.text('SignUp'));
    await tester.pumpAndSettle();

    expect(find.byType(SignUpPage), findsOneWidget);
  });
}
