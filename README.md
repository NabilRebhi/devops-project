# simple_monitor:

Simple monitor is a flutter desktop application for Linux.

This is a simple and small network and system monitoring application designed for small local networks.


## Components:

The application sores data(administrator "login" and "pass", and terminals data) into a PostgreSQL data base.

You can test it locally in a docker conainer using my custom image: "nabilrebhi/flutter-test". It is also written in the Dockerfile in "devops-project/testing-folder/flutter-test"

I also tried to create a custom image for a docker container having a GUI to run the application in it, you can check the Dockerfile in "devops-project/testing-folder/Dockerfile-for-GUI-for-flutter".

