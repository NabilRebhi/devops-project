#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <libpq-fe.h>

int check_host(char *hostname)
{
    char command[100];
    sprintf(command, "ping -c 1 %s > /dev/null", hostname);
    if (system(command) == 0)
    {
        return 1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    PGconn *conn = PQconnectdb("host=192.168.8.254 dbname=storage user=simplemonitor password=A12GHF@phFF675");

    if (PQstatus(conn) == CONNECTION_BAD)
    {
        fprintf(stderr, "Connection to database failed: %s\n", PQerrorMessage(conn));
        PQfinish(conn);
        exit(1);
    }

    PGresult *res = PQexec(conn, "SELECT ip_address FROM terminals");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s\n", PQerrorMessage(conn));
        PQclear(res);
        PQfinish(conn);
        exit(1);
    }

    int num_rows = PQntuples(res);
    for (int i = 0; i < num_rows; i++)
    {
        char *ip_address = PQgetvalue(res, i, 0);
        char query[1024];
        sprintf(query, "UPDATE terminals SET status='%s' WHERE ip_address='%s'", (check_host(ip_address) ? "active" : "inactive"), ip_address);
        PGresult *update_res = PQexec(conn, query);
        if (PQresultStatus(update_res) != PGRES_COMMAND_OK)
        {
            fprintf(stderr, "Update failed for IP address %s: %s\n", ip_address, PQerrorMessage(conn));
        }
        PQclear(update_res);
    }

    PQclear(res);
    PQfinish(conn);

    return 0;
}

