#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libpq-fe.h>

int check_port(char *hostname, char *port)
{
    int sockfd;
    struct sockaddr_in serv_addr;
    // Create a socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        return 2;
    }

    // Set the server address
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(port));          // port 22 for SSH
    serv_addr.sin_addr.s_addr = inet_addr(hostname); // destination host IP

    // Attempt to connect to the server
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }

    close(sockfd);
}

int main(int argc, char *argv[])
{

    PGconn *conn = PQconnectdb("host=192.168.8.254 dbname=storage user=simplemonitor password=A12GHF@phFF675");

    if (PQstatus(conn) == CONNECTION_BAD)
    {
        fprintf(stderr, "Connection to database failed: %s\n", PQerrorMessage(conn));
        PQfinish(conn);
        exit(1);
    }

    PGresult *res = PQexec(conn, "SELECT ip_address FROM terminals");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s\n", PQerrorMessage(conn));
        PQclear(res);
        PQfinish(conn);
        exit(1);
    }

    int num_rows = PQntuples(res);
    for (int i = 0; i < num_rows; i++)
    {
        char *ip_address = PQgetvalue(res, i, 0);
        char query[1024];
        sprintf(query, "UPDATE terminals SET http_status='%s' WHERE ip_address='%s'", (check_port(ip_address, "80") ? "closed" : "opened"), ip_address);
        PGresult *res = PQexec(conn, query);
        if (PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            fprintf(stderr, "Update failed: %s\n", PQerrorMessage(conn));
            PQclear(res);
            PQfinish(conn);
            exit(1);
        }

        sprintf(query, "UPDATE terminals SET ssh_status='%s' WHERE ip_address='%s'", (check_port(ip_address, "22") ? "closed" : "opened"), ip_address);
        res = PQexec(conn, query);
        if (PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            fprintf(stderr, "Update failed: %s\n", PQerrorMessage(conn));
            PQclear(res);
            PQfinish(conn);
            exit(1);
        }
        
    }
    PQclear(res);
    PQfinish(conn);
    return 0;
}
