# Prepare the envirement

```
1- sudo apt update && sudo apt upgrade -y
2- sudo apt install gcc -y 
3- sudo apt install libpq-dev

```

# Compile 

```
gcc -I /usr/include/postgresql/ input_file.c -o output_file -lpq

```

# To execute we need only to execute the output program of the "check.c"

```
./check_output_program

```