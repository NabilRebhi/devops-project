import 'package:flutter/material.dart';
import 'package:simplemonitor/screens/dashboard.dart';
import 'package:simplemonitor/setting_widget/add_terminal_widget.dart';
import 'package:simplemonitor/setting_widget/delete_terminal_widget.dart';
import 'package:simplemonitor/setting_widget/profile_widget.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late bool profileButtonPressed = false;
  late String menu = "profile";

  Widget mainWidget(context, String text) {
    late Widget result = SizedBox(
      height: double.infinity,
      width: MediaQuery.of(context).size.width * 0.7,
    );
    if (text == "profile") {
      /*
      *****************************************************************profile widget
      */
      result = SizedBox(
        height: double.infinity,
        width: MediaQuery.of(context).size.width * 0.7,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text(
                      "Profile",
                      style:
                          TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            profileButtonPressed = !profileButtonPressed;
                          });
                        },
                        icon: const Icon(
                          Icons.edit,
                        ))
                  ],
                ),
                const SizedBox(
                  height: 100,
                ),
                ProfileWidget.notEditing(context, profileButtonPressed).first,
              ],
            ),
          ),
        ),
      );
    } else if (text == "add terminals") {
      /*
      *************************************add terminal widget
       */
      result = SizedBox(
        height: double.infinity,
        width: MediaQuery.of(context).size.width * 0.7,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Add Terminals",
                  style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 100,
                ),
                AddTerminalWidget.addTerminals(context: context).first,
              ],
            ),
          ),
        ),
      );
    } else if (text == "delete terminals") {
      /**
       ************************************delete terminals widget
       */
      result = SizedBox(
        height: double.infinity,
        width: MediaQuery.of(context).size.width * 0.7,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Delete Terminals",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 100,
                ),
                DeleteTerminalWidget.deleteTerminals(context: context).first,
              ],
            ),
          ),
        ),
      );
    }
    return result;
  }

  Widget sideWidget(context) {
    return SizedBox(
      height: double.infinity,
      width: MediaQuery.of(context).size.width * 0.3,
      child: DecoratedBox(
        decoration: const BoxDecoration(color: Colors.black12),
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: double.infinity,
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.blue,
                ),
                child: TextButton(
                  child: const Text(
                    "profile",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      menu = "profile";
                    });
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: double.infinity,
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.blue,
                ),
                child: TextButton(
                  child: const Text(
                    "add terminals",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      menu = "add terminals";
                    });
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 50,
              width: double.infinity,
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Colors.blue,
                ),
                child: TextButton(
                  child: const Text(
                    "delete terminals",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    setState(() {
                      menu = "delete terminals";
                    });
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text("Settings"),
          actions: [
            IconButton(
              icon: const Icon(Icons.dashboard),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const DashBoard()));
              },
            ),
          ]),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Row(
          children: [
            sideWidget(context),
            mainWidget(context, menu),
          ],
        ),
      ),
    );
  }
}
