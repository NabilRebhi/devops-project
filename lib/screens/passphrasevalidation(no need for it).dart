// ignore_for_file: use_build_context_synchronously, file_names

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/functions/passphrase(no%20need%20for%20it).dart';
import 'package:simplemonitor/screens/dashboard.dart';


class PassphrasePage extends StatefulWidget {
  const PassphrasePage({super.key});

  @override
  State<PassphrasePage> createState() => _PassphrasePageState();
}

class _PassphrasePageState extends State<PassphrasePage> {
  final TextEditingController _passphrase = TextEditingController();
  late bool isHidden = true;
  late int result = 2;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 54, 152, 244),
      body: Center(
        child: DecoratedBox(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(60)),
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.06,
                MediaQuery.of(context).size.height * 0.1,
                MediaQuery.of(context).size.width * 0.06,
                MediaQuery.of(context).size.height * 0.1),
            child: Column(
              children: [
                const Text(
                  "Validate your passphrase",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    maxWidth: 250,
                  ),
                  child: TextFormField(
                    obscureText: isHidden,
                    controller: _passphrase,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                      prefixIcon: const Icon(Icons.password_rounded),
                      suffixIcon: isHidden
                          ? IconButton(
                              onPressed: () {
                                setState(() {
                                  isHidden = !isHidden;
                                });
                              },
                              icon: const Icon(Icons.visibility_off))
                          : IconButton(
                              onPressed: () {
                                setState(() {
                                  isHidden = !isHidden;
                                });
                              },
                              icon: const Icon(Icons.visibility)),
                      filled: true,
                      hintStyle: TextStyle(color: Colors.grey[800]),
                      hintText: "Passphrase",
                      fillColor: Colors.white70,
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                DecoratedBox(
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 54, 152, 244),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () async {
                     //login function
                     await Passphrase.loadLocalPassphrase(_passphrase.text).then((value) {
                      result = value;
                     });
                     if (result == 0){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const DashBoard()));
                     }else{
                      return alert(
                              context,
                              title: const Text("Passphrase Don't match!"),
                              content: const Text("The given passphrase is not the same as the stored one."),
                              textOK:const Text("GOT IT"),
                              );
                     }

                    },
                    child: const Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 70,
                        vertical: 10,
                      ),
                      child: Text(
                        "Sign In",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.02,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
