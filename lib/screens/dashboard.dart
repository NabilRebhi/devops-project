import 'dart:async';
import 'package:flutter/material.dart';
import 'package:simplemonitor/functions/status_colors.dart';
import 'package:simplemonitor/postgresutil/postgres_terminal_services.dart';
import 'package:simplemonitor/screens/settings.dart';
import 'package:simplemonitor/screens/signin.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  late Timer _refreshTimer;
  late String getTerminalsResult = "none";
  late List<TableRow> rows = [];
  late int length = 0;

  Future<void> _refresh() async {
    await PostgresTerminalService.getTerminals()
        .then((value) {
          setState(() {
            getTerminalsResult = value;
          });
        });
    if (getTerminalsResult == "done") {
      setState(() {
        length = PostgresTerminalService.terminals.getRow()!.length;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    // Set up a timer to refresh the list every 5 seconds
    _refreshTimer = Timer.periodic(const Duration(seconds: 5), (timer) {
      _refresh();
    });
  }

  @override 
  void dispose() {
    // Cancel the timer when the widget is disposed
    _refreshTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (getTerminalsResult == "no terminals found") {
      return Scaffold(
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.warning_rounded,
                    size: 150,
                    color: Colors.orange,
                  ),
                  const Text(
                    "There are no terminals added to the database",
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.orange,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SettingsPage()));
                    },
                    child: const Text("ADD TERMINALS"),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } else if (getTerminalsResult == "done") {
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 20,
          title: const Text("Dashboard"),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SettingsPage()));
              },
              icon: const Icon(Icons.settings),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.05,
            ),
            IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SignInPage()));
              },
              icon: const Icon(Icons.logout),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.02,
            ),
          ],
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                const Center(
                  child: Text(
                    "Stats",
                    style: TextStyle(
                      fontSize: 60,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.1,
                ),
                Table(
                  border: TableBorder.all(color: Colors.black),
                  children: [
                    const TableRow(
                      children: [
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "IP",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "Name",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "Location",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "Status",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "HTTP Status",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        DecoratedBox(
                          decoration: BoxDecoration(
                            color: Colors.black38,
                          ),
                          child: Text(
                            "SSH Status",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    for (int index = 0; index < length; index++)
                      (TableRow(
                        children: [
                          Text(
                            PostgresTerminalService.terminals
                                .getRow()!
                                .elementAt(index)
                                .elementAt(0)
                                .toString(),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            PostgresTerminalService.terminals
                                .getRow()!
                                .elementAt(index)
                                .elementAt(1)
                                .toString(),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            PostgresTerminalService.terminals
                                .getRow()!
                                .elementAt(index)
                                .elementAt(2)
                                .toString(),
                            textAlign: TextAlign.center,
                          ),
                          DecoratedBox(
                            decoration:  BoxDecoration(
                              color: StatusColors.check(status: PostgresTerminalService.terminals.getRow()!.elementAt(index).elementAt(3).toString()),
                            ),
                            child: Text(
                              PostgresTerminalService.terminals
                                  .getRow()!
                                  .elementAt(index)
                                  .elementAt(3)
                                  .toString(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: StatusColors.check(status: PostgresTerminalService.terminals.getRow()!.elementAt(index).elementAt(4).toString()),
                            ),
                            child: Text(
                              PostgresTerminalService.terminals
                                  .getRow()!
                                  .elementAt(index)
                                  .elementAt(4)
                                  .toString(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          DecoratedBox(
                            decoration: BoxDecoration(
                              color: StatusColors.check(status: PostgresTerminalService.terminals.getRow()!.elementAt(index).elementAt(5).toString()),
                            ),
                            child: Text(
                              PostgresTerminalService.terminals
                                  .getRow()!
                                  .elementAt(index)
                                  .elementAt(5)
                                  .toString(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      )),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: DecoratedBox(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.sync,
                  size: 150,
                  color: Colors.blue,
                ),
                Text(
                  "Loading",
                  style: TextStyle(
                    fontSize: 50,
                    color: Colors.blue,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
