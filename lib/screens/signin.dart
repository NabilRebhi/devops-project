// ignore_for_file: use_build_context_synchronously

import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';
import 'package:simplemonitor/screens/dashboard.dart';
import 'package:simplemonitor/screens/signup.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _password = TextEditingController();
  late bool isHidden = true;
  late String result;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  late bool check;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 54, 152, 244),
      body: Center(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(60),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 5.0,
                spreadRadius: 5.0,
                offset: Offset(7.0, 7.0),
              ),
            ],
          ),
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.06,
                MediaQuery.of(context).size.height * 0.1,
                MediaQuery.of(context).size.width * 0.06,
                MediaQuery.of(context).size.height * 0.1),
            child: Form(
              key: _form,
              child: Column(
                children: [
                  const Text(
                    "Sign In",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  //login form
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 250,
                    ),
                    child: TextFormField(
                      controller: _userName,
                      validator: (validator) {
                        if (_userName.text.isEmpty) {
                          return "Empty Username Field!";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.person),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Username",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 250,
                    ),
                    child: TextFormField(
                      obscureText: isHidden,
                      controller: _password,
                      validator: (validator) {
                        if (_password.text.isEmpty) {
                          return "Empty Password Field!";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.password_rounded),
                        suffixIcon: isHidden
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden = !isHidden;
                                  });
                                },
                                icon: const Icon(Icons.visibility_off))
                            : IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden = !isHidden;
                                  });
                                },
                                icon: const Icon(Icons.visibility)),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Passwsord",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  DecoratedBox(
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 54, 152, 244),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: TextButton(
                      onPressed: () async {
                        setState(() {
                          check = _form.currentState!.validate();
                        });
                        if (check == true) {
                          //login function
                          await PostgresClientService.signinClient(
                                  _userName.text, _password.text)
                              .then((value) {
                            setState(() {
                              result = value;
                            });
                          });
                          if (result == "done") {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const DashBoard()));
                          } else {
                            return alert(
                              context,
                              title: const Text("Wrong username or password!"),
                              content: const Text(
                                  "We cannot find an account with these credentials."),
                              textOK: const Text("GOT IT"),
                            );
                          }
                        }
                      },
                      child: const Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 70,
                          vertical: 10,
                        ),
                        child: Text(
                          "SignIn",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("You don't have an account? "),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignUpPage()));
                        },
                        child: const Text("SignUp"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
