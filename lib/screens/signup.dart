// ignore_for_file: use_build_context_synchronously

import 'package:alert_dialog/alert_dialog.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';
import 'package:simplemonitor/screens/signin.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _repeatedPassword = TextEditingController();

  late bool isHidden = true;
  late bool isHidden1 = true;
  late bool check;
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  late String result;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 54, 152, 244),
      body: Center(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(60),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 5.0,
                spreadRadius: 5.0,
                offset: Offset(7.0, 7.0),
              ),
            ],
          ),
          child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(
                MediaQuery.of(context).size.width * 0.08,
                MediaQuery.of(context).size.height * 0.05,
                MediaQuery.of(context).size.width * 0.08,
                MediaQuery.of(context).size.height * 0.05),
            child: Form(
              key: _form,
              child: Column(
                children: [
                  const Text(
                    "Sign Up",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 300,
                    ),
                    child: TextFormField(
                      controller: _userName,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.person),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Username",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 300,
                    ),
                    child: TextFormField(
                      controller: _email,
                      validator: (validator) {
                        if (EmailValidator.validate(_email.text) == false) {
                          return "Email not valid !";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.email_rounded),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Email address",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 300,
                    ),
                    child: TextFormField(
                      obscureText: isHidden,
                      controller: _password,
                      validator: (validator) {
                        if (validator!.isEmpty) return 'Empty !';
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.password_rounded),
                        suffixIcon: isHidden
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden = !isHidden;
                                  });
                                },
                                icon: const Icon(Icons.visibility_off))
                            : IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden = !isHidden;
                                  });
                                },
                                icon: const Icon(Icons.visibility)),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Passwsord",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxWidth: 300,
                    ),
                    child: TextFormField(
                      obscureText: isHidden1,
                      controller: _repeatedPassword,
                      validator: (validator) {
                        if (validator!.isEmpty) return 'Empty !';
                        if (validator != _password.text) {
                          return 'The passwords do not match !';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        prefixIcon: const Icon(Icons.password_rounded),
                        suffixIcon: isHidden1
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden1 = !isHidden1;
                                  });
                                },
                                icon: const Icon(Icons.visibility_off))
                            : IconButton(
                                onPressed: () {
                                  setState(() {
                                    isHidden1 = !isHidden1;
                                  });
                                },
                                icon: const Icon(Icons.visibility)),
                        filled: true,
                        hintStyle: TextStyle(color: Colors.grey[800]),
                        hintText: "Confirm Passwsord",
                        fillColor: Colors.white70,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  DecoratedBox(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: const Color.fromARGB(255, 54, 152, 244),
                    ),
                    child: TextButton(
                      onPressed: () async {
                        setState(() {
                          check = _form.currentState!.validate();
                        });
                        if (check == true) {
                          //register function
                          await PostgresClientService.signupClient(
                                  _userName.text, _email.text, _password.text)
                              .then((value) {
                            setState(() {
                              result = value;
                            });
                          });
                          if (result == "done") {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignInPage()),
                            );
                          } else {
                            return alert(
                              context,
                              title: const Text("User already exists!"),
                              content: const Text(
                                  "please choose another username and email address"),
                              textOK: const Text("GOT IT"),
                            );
                          }
                        }
                      },
                      child: const Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 65,
                          vertical: 10,
                        ),
                        child: Text(
                          "Submit",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("You have an account? "),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignInPage()));
                        },
                        child: const Text("SignIn"),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
