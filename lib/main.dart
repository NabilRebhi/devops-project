import 'dart:io';
import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';

import 'package:simplemonitor/screens/signin.dart';

import 'package:window_size/window_size.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isWindows || Platform.isLinux) {
    setWindowTitle('Simple Monitor');
    setWindowMaxSize(Size.infinite);
    setWindowMinSize(const Size(800, 600));
  }

  await PostgresClientService.checkQueriesAfterCreatingTables().then((value) {
    if (value == "done") {
      runApp(const MyApp());
    } else {
      runApp(const ErrorWidget());
    }
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple monitoring software',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SignInPage(),
    );
  }
}

class ErrorWidget extends StatelessWidget {
  const ErrorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple monitoring software',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.warning_rounded,
                    size: 150,
                    color: Colors.red,
                  ),
                  const Text(
                    "A Database Error has accured!!",
                    style: TextStyle(
                      fontSize: 50,
                      color: Colors.red,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      exit(0);
                    },
                    child: const Text(
                      'Exit',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
