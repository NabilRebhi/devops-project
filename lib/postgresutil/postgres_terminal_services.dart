import 'package:postgres/postgres.dart';
import 'package:simplemonitor/datafromserver/terminals_data.dart';
import 'package:simplemonitor/postgresutil/postgres_config.dart';

class PostgresTerminalService {
  static TerminalsData terminals = TerminalsData();
  PostgresTerminalService();

  static final PostgresConfig config = PostgresConfig();

  static Future<String> addTerminal(
      {required String ipAddress,
      required String name,
      required String location,
      String? parent}) async {
    late String result = "none";

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();

    await conn.query(
        "SELECT ip_address FROM \"terminals\" WHERE ip_address=@ip",
        substitutionValues: {"ip": ipAddress}).then((value) {
      if (value.affectedRowCount != 0) {
        result = "error";
      } else {
        result = "done";
      }
    });

    if (result == "done") {
      try {
        await conn.query(
            "INSERT INTO \"terminals\" (ip_address, name, location,status, http_status,ssh_status,parent) VALUES (@ip,@name,@location,@status,@http_status,@ssh_status,@parent)",
            substitutionValues: {
              "ip": ipAddress,
              "name": name,
              "location": location,
              "status": 'pending',
              "http_status": 'pending',
              "ssh_status": 'pending',
              "parent": parent
            });
      } catch (e) {
        result = e.toString();
      }
    }
    await conn.close();

    return result;
  }

  static Future<String> getTerminals() async {
    late String result = "none";

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();

    try {
      await conn.query("SELECT * FROM \"terminals\"").then((value) {
        if (value.affectedRowCount != 0) {
          
          result = "done";
          terminals.setRow(value);
        } else if (value.isEmpty) {
          
          result = "no terminals found";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();
    
    return result;
  
  }

  static Future<String> deleteTerminal({required String ipAddress}) async {

    late String result = "none";

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();

    try {
      await conn.query("DELETE FROM \"terminals\" WHERE ip_address=@ip",substitutionValues: {
        "ip": ipAddress
      }).then((value) {
        if (value.affectedRowCount != 0) {
          result = "done";
        }
      });
    } catch (e) {
      result = e.toString();
    }

    await conn.close();


    return result;
  }




}
  
