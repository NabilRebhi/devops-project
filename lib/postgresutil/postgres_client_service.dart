import 'package:simplemonitor/datafromserver/user_data.dart';
import 'package:postgres/postgres.dart';
import 'package:simplemonitor/functions/encryption.dart';
import 'package:simplemonitor/postgresutil/postgres_config.dart';

class PostgresClientService {
  static UserData userData = UserData();
  static final PostgresConfig config =
      PostgresConfig();

  PostgresClientService();

  static Future<String> createUsersTableIfNotExist() async {
    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    late String result = "none";
    try {
      await conn.open();
      await conn.query(
          "CREATE TABLE IF NOT EXISTS users (username TEXT NOT NULL,password TEXT NOT NULL  ,email TEXT NOT NULL, PRIMARY KEY (username))");
      result = "done";
      await conn.close();
    } catch (e) {
      result = e.toString();
    }
    return result;
  }

  static Future<String> createTerminalsTableIfNotExist() async {
    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    late String result = "none";
    try {
      await conn.open();
      await conn.query(
          "CREATE TABLE IF NOT EXISTS terminals (ip_address TEXT NOT NULL,name TEXT NOT NULL,location TEXT NOT NULL,status TEXT NOT NULL,http_status TEXT NOT NULL,ssh_status TEXT NOT NULL,parent TEXT REFERENCES terminals(ip_address),PRIMARY KEY (ip_address))");
      result = "done";
      await conn.close();
    } catch (e) {
      result = e.toString();
    }
    return result;
  }

  static Future<String> checkQueriesAfterCreatingTables() async {
    String usersTable = "";
    String terminalsTable = "";
    await PostgresClientService.createUsersTableIfNotExist()
        .then((value) => usersTable = value);

    await PostgresClientService.createTerminalsTableIfNotExist()
        .then((value) => terminalsTable = value);

    if(usersTable == "done" && terminalsTable == "done"){
      return "done";
    } else {
      return "error on creating table";
    }
  }

  static Future<String> signinClient(String username, String password) async {
    String resultCon = "none";
    String encryptedpass = Encryption.generateMd5(password);

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();

    await conn.query(
        "SELECT username,email FROM \"users\" WHERE username=@user AND password=@pass",
        substitutionValues: {
          "user": username,
          "pass": encryptedpass
        }).then((value) {
      if (value.affectedRowCount != 0) {
        userData.setRow(value);
        resultCon = "done";
      } else {
        resultCon = "error";
      }
    });
    await conn.close();
    return resultCon;
  }

  static Future<String> signupClient(
      String username, String email, String password) async {
    late String result = "none";
    String encryptedpass = Encryption.generateMd5(password);

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();

    await conn.query(
        "SELECT username FROM \"users\" WHERE username=@user OR email=@email",
        substitutionValues: {
          "user": username,
          "email": email,
        }).then((value) {
      if (value.affectedRowCount != 0) {
        result = "error";
      } else {
        result = "done";
      }
    });

    if (result == "done") {
      await conn.query(
          "INSERT INTO \"users\" (username, email, password) VALUES (@user,@email,@pass)",
          substitutionValues: {
            "user": username,
            "email": email,
            "pass": encryptedpass
          });
    }
    await conn.close();
    return result;
  }

  static Future<String> editProfile({required String previousUsername,required String newUsername, required String email,required String password}) async {
    String resultCon = "none";
    String encryptedpass = Encryption.generateMd5(password);

    var conn = PostgreSQLConnection(config.dbHost, config.dbPort, config.dbName,
        username: config.dbUsername, password: config.dbPassword);
    await conn.open();



    await conn.query(
        "SELECT username,email FROM \"users\" WHERE username=@user AND password=@pass",
        substitutionValues: {
          "user": previousUsername,
          "pass": encryptedpass
        }).then((value) {
      if (value.affectedRowCount != 0) {
        userData.setRow(value);
        resultCon = "done";
      } else {
        resultCon = "error";
      }
    });


    if(resultCon == "done"){
      await conn.query(
        "UPDATE \"users\" SET username = @new, email = @email WHERE username =@previous",
        substitutionValues: {
          "new": newUsername,
          "email": email,
          "previous": previousUsername
        }).then((value) {
      if (value.affectedRowCount != 0) {
        resultCon = "done";
      } else {
        resultCon = "error";
      }
    });
    }
    await conn.close();
    return resultCon;
  }
}
