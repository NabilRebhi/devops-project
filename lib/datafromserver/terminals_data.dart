class TerminalsData {
  TerminalsData();

  List? _rowForTerminals;

  List? getRow() {
    return _rowForTerminals;
  }

  setRow(List? rowForTerminals) {
    _rowForTerminals = rowForTerminals;
  }
}
