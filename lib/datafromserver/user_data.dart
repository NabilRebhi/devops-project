class UserData {
  UserData();

  List? _rowForUser;

  List? getRow() {
    return _rowForUser;
  }

  setRow(List? rowForUser) {
    _rowForUser = rowForUser;
  }
}
