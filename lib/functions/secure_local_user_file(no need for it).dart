// ignore_for_file: file_names

import 'package:file_cryptor/file_cryptor.dart';
import 'package:system/system.dart';

class SecureLocalUserFile {
  static Future<void> encryptFile(String inputFileName, String outputFileName) async {
    FileCryptor fileCryptor = FileCryptor(
      key: "zapA4F7MQod1@8ijzccOjcu1ni0ebGbD",
      iv: 16,
      dir: "./assets/app/.pass",
      // useCompress: true,
    );
    await fileCryptor.encrypt(
        inputFile: inputFileName, outputFile: outputFileName);
    System.invoke("rm ./app/.pass/passphrase.txt");
  }

  static Future<void> decryptFile(
      String inputFileName, String outputFileName) async {
    FileCryptor fileCryptor = FileCryptor(
      key: "zapA4F7MQod1@8ijzccOjcu1ni0ebGbD",
      iv: 16,
      dir: "./assets/app/.pass",
      // useCompress: true,
    );
    await fileCryptor.decrypt(inputFile: inputFileName, outputFile: outputFileName);

  }
}
