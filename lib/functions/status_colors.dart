import 'package:flutter/material.dart';

class StatusColors{
  StatusColors();



  static Color check({required String status}){
    if (status == "active" || status == "opened"){
      return Colors.green;
    }else if (status == "pending"){
      return Colors.black38;
    }else {
      return Colors.red;
    }
  }


}