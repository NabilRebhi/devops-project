// ignore_for_file: file_names

import 'dart:convert';
import 'dart:io';

import 'package:simplemonitor/functions/encryption.dart';
import 'package:simplemonitor/functions/secure_local_user_file(no%20need%20for%20it).dart';

import 'package:system/system.dart';

class Passphrase{

  static Future<bool> localPassExists() async {
    bool exists = false;


    System.invoke("mkdir ./app/.pass");

  

    File file = File('./app/.pass/passphrase.aes');

    await file.length().then((value) {
      if ( value > 0 ){
        exists = true;
      }
    }).onError((error, stackTrace) {});

    return exists;
  }


  static Future<bool> createlocalPassphrase(String pass) async{
    late bool result = false;
    File file = File('./app/.pass/passphrase.txt');

    var f = file.openWrite();

    f.write(Encryption.generateMd5(pass).toString());
    f.close();
    
    await SecureLocalUserFile.encryptFile('passphrase.txt','passphrase.aes');

    return result;
  }


  static Future<int> loadLocalPassphrase(String pass) async{
    late int result = 2;
    String? storedPass;
    String givenPass = Encryption.generateMd5(pass);

    await SecureLocalUserFile.decryptFile('passphrase.aes', 'passphrase.txt');

    File file = File('./app/.pass/passphrase.txt');
    Stream<String> line = file.openRead().transform(utf8.decoder).transform(const LineSplitter());

    await line.elementAt(0).then((String value) => storedPass = value);

    if (storedPass == givenPass){
      result = 0;
    }


    System.invoke("rm ./app/.pass/passphrase.txt");

    return result;

  }



}