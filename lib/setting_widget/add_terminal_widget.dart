import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_terminal_services.dart';
import 'package:simplemonitor/screens/dashboard.dart';

class AddTerminalWidget {
  AddTerminalWidget();

  static List<dynamic> addTerminals({context}) {
    List<dynamic> result = [];
    final GlobalKey<FormState> form = GlobalKey<FormState>();
    TextEditingController ipAddress = TextEditingController();
    TextEditingController name = TextEditingController();
    TextEditingController location = TextEditingController();
    TextEditingController parent = TextEditingController();
    result = [
      Form(
        key: form,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text("   IP Address:"),
            TextFormField(
              controller: ipAddress,
              validator: (validator) {
                if (validator!.isEmpty) {
                  return "Empty Field";
                }
                return null;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                hintStyle: TextStyle(color: Colors.grey[800]),
                hintText: "IP Address",
                fillColor: Colors.white70,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text("   name:"),
            TextFormField(
              controller: name,
              validator: (validator) {
                if (validator!.isEmpty) {
                  return "Empty Field";
                }
                return null;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                hintStyle: TextStyle(color: Colors.grey[800]),
                hintText: "name",
                fillColor: Colors.white70,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text("   location:"),
            TextFormField(
              controller: location,
              validator: (validator) {
                if (validator!.isEmpty) {
                  return "Empty Field";
                }
                return null;
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                hintStyle: TextStyle(color: Colors.grey[800]),
                hintText: "location",
                fillColor: Colors.white70,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text("   parent:"),
            TextFormField(
              controller: parent,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                hintStyle: TextStyle(color: Colors.grey[800]),
                hintText: "parent",
                fillColor: Colors.white70,
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Center(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: TextButton(
                    onPressed: () async {
                      bool check = form.currentState!.validate();
                      if (check == true) {
                        late String adding;
                        if (parent.text == "") {
                          await PostgresTerminalService.addTerminal(
                            ipAddress: ipAddress.text,
                            name: name.text,
                            location: location.text,
                          ).then((value) => adding = value);
                          ipAddress.text = "";
                          name.text = "";
                          location.text = "";
                          Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DashBoard()));
                        } else {
                          await PostgresTerminalService.addTerminal(
                            ipAddress: ipAddress.text,
                            name: name.text,
                            location: location.text,
                            parent: parent.text,
                          ).then((value) => adding = value);
                          ipAddress.text = "";
                          name.text = "";
                          location.text = "";
                          parent.text = "";
                          Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DashBoard()));
                        }
                        if (adding != "done") {
                          return alert(
                            context,
                            title: const Text("Error!"),
                            content: const Text(
                                "This is an exisiting terminal or the parent does not exists"),
                            textOK: const Text("GOT IT"),
                          );
                        }
                      }
                    },
                    child: const Text(
                      "ADD",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    ];
    return result;
  }
}
