import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_client_service.dart';
import 'package:simplemonitor/screens/settings.dart';

class ProfileWidget {
  ProfileWidget();

  static List<Widget> notEditing(context, bool pressed) {
    late List<Widget> result;
    if (pressed == false) {
      result = [
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Username:"),
                Text(PostgresClientService.userData
                    .getRow()!
                    .elementAt(0)
                    .elementAt(0)
                    .toString())
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text("Email address:"),
                Text(PostgresClientService.userData
                    .getRow()!
                    .elementAt(0)
                    .elementAt(1)
                    .toString())
              ],
            ),
          ],
        ),
      ];
    } else {
      final GlobalKey<FormState> form = GlobalKey<FormState>();
      TextEditingController username = TextEditingController();
      TextEditingController email = TextEditingController();
      TextEditingController password = TextEditingController();
      late String update = "none";
      result = [
        Form(
          key: form,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text("   Username:"),
              TextFormField(
                controller: username,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  prefixIcon: const Icon(Icons.person),
                  filled: true,
                  hintStyle: TextStyle(color: Colors.grey[800]),
                  hintText: "Username",
                  fillColor: Colors.white70,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              const Text("   Email:"),
              TextFormField(
                controller: email,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  prefixIcon: const Icon(Icons.email),
                  filled: true,
                  hintStyle: TextStyle(color: Colors.grey[800]),
                  hintText: "Email",
                  fillColor: Colors.white70,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              const Text("   Verify Password:"),
              TextFormField(
                obscureText: true,
                controller: password,
                validator: (validator) {
                  if (validator!.isEmpty) return 'Empty !';
                  return null;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  prefixIcon: const Icon(Icons.password_rounded),
                  filled: true,
                  hintStyle: TextStyle(color: Colors.grey[800]),
                  hintText: "Verify Passwsord",
                  fillColor: Colors.white70,
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Center(
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: TextButton(
                      onPressed: () async {
                        await PostgresClientService.editProfile(
                                previousUsername: PostgresClientService.userData
                                    .getRow()!
                                    .elementAt(0)
                                    .elementAt(0)
                                    .toString(),
                                newUsername: username.text,
                                email: email.text,
                                password: password.text)
                            .then((value) => update = value);
                        if (update == "done") {
                          await PostgresClientService.signinClient(
                                  username.text, password.text)
                              .then((value) => update = value);
                        }
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const SettingsPage()));
                      },
                      child: const Text(
                        "UPDATE",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ];
    }
    return result;
  }
}
