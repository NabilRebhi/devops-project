import 'package:alert_dialog/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:simplemonitor/postgresutil/postgres_terminal_services.dart';
import 'package:simplemonitor/screens/dashboard.dart';

class DeleteTerminalWidget {
  DeleteTerminalWidget();

  static List<dynamic> deleteTerminals({context}) {
    String deleteResult = "none";
    List<dynamic> result = [];
    final GlobalKey<FormState> form = GlobalKey<FormState>();
    TextEditingController ipAddress = TextEditingController();
    result = [
      Form(
        key: form,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text("   IP Address:"),
            TextFormField(
              controller: ipAddress,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                filled: true,
                hintStyle: TextStyle(color: Colors.grey[800]),
                hintText: "IP Address",
                fillColor: Colors.white70,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              "  By deleting a terminal, all the childs of it will be deleted",
              style: TextStyle(color: Colors.red),
            ),
            const SizedBox(
              height: 50,
            ),
            Center(
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: TextButton(
                    onPressed: () async {
                      await PostgresTerminalService.deleteTerminal(
                              ipAddress: ipAddress.text)
                          .then((value) {
                        deleteResult = value;
                      });
                      if (deleteResult == "done") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const DashBoard()));
                      }
                      if (deleteResult != "done") {
                        return alert(
                          context,
                          title: const Text("Error!"),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              Text("We can't delete this terminal."),
                              Text(
                                  "Others depend on this terminal or the ip address written is invalide"),
                            ],
                          ),
                          textOK: const Text("GOT IT"),
                        );
                      }
                    },
                    child: const Text(
                      "DELETE",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    ];
    return result;
  }
}
